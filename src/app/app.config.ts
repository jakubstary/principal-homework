import { Injectable } from '@angular/core';

export class AppConfig {
    public readonly apiUrl: string = 'https://jsonplaceholder.typicode.com/';
    public readonly translationsPath: string = '../assets/translations.json';
}
