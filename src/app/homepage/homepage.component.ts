import { Component, OnInit } from '@angular/core';
import { Post } from '../_models/index';
import { PostService, AlertService, TranslateService } from '../_services/index';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
    templateUrl: './homepage.component.html',
    styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {
    posts: Post[] = [];

    constructor(private postService: PostService,
        private alertService: AlertService,
        private translateService: TranslateService,
        public dialog: MdDialog) {}

    ngOnInit() {
        this.postService.getAllPosts().toPromise().then(posts => {
            this.posts = posts;
        }).catch(exception => {
            this.alertService.error(this.translateService.translate('Something went wrong!'));
        });
    }

    public openNewPostDialog() {
        const dialogRef = this.dialog.open(NewPostDialog);
    }
}


// New Post Dialog
@Component({
  templateUrl: 'newpost.dialog.component.html',
})
export class NewPostDialog {
    newPost: Post = new Post();

    constructor(
        public dialogRef: MdDialogRef<NewPostDialog>,
        private postService: PostService,
        private alertService: AlertService,
        private translateService: TranslateService) {}

    onSubmit() {
        this.newPost.userId = 11;
        this.newPost.id = 101;
        this.postService.newPost(this.newPost).subscribe(data => {
            this.alertService.success(this.translateService.translate('New post saved successfully'));
            this.dialogRef.close(true);
        }, error => {
            this.alertService.error(error._body);
        });
    }
}
