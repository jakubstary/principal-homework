import { Component, OnInit } from '@angular/core';
import { TranslateService } from '../_services/index';
import { Lang } from '../_models/index';

@Component({
    selector: 'lang',
    templateUrl: 'lang.component.html',
    styleUrls: ['./lang.component.css']
})
export class LangComponent {
    lang: Lang;

    constructor(private translateService: TranslateService) {}

    public changeLanguage() {
        this.translateService.changeLang();
    }
}
