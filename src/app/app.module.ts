import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppConfig } from './app.config';
import 'hammerjs';

// Services
import { PostService, AlertService, TranslateService } from './_services/index';

// Components
import { AppComponent } from './app.component';
import { HomepageComponent, NewPostDialog } from './homepage/index';
import { PostComponent, DeleteDialog, EditDialog } from './post/index';
import { AlertComponent } from './alert/index';
import { LangComponent } from './lang/index';

// Routing
const appRoutes: Routes = [
    { path: 'post/:id', component: PostComponent },
    { path: '', component: HomepageComponent},
    { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    PostComponent,
    AlertComponent,
    LangComponent,
    DeleteDialog,
    EditDialog,
    NewPostDialog
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    HttpModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  providers: [
    AppConfig,
    PostService,
    AlertService,
    TranslateService
  ],
  entryComponents: [
    DeleteDialog,
    EditDialog,
    NewPostDialog
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
