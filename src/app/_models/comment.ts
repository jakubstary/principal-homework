export class Comment {
    from: string;
    to: string;
    subject: string;
    html: string;
}
