import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';
import { Alert } from '../_models/alert';

@Injectable()
export class AlertService {
    private alert = new Subject<Alert>();
    private keepAfterNavigationChange = false;

    constructor(private router: Router) {
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                if (this.keepAfterNavigationChange) {
                    this.keepAfterNavigationChange = false;
                } else {
                    this.clear();
                }
            }
        });
    }

    success(message: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.alert.next({ type: 'success', text: message });
        window.scrollTo(0, 0);
    }

    error(message: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.alert.next({ type: 'error', text: message });
        window.scrollTo(0, 0);
    }

    warning(message: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        this.alert.next({ type: 'warning', text: message });
        window.scrollTo(0, 0);
    }

    getMessage(): Observable<Alert> {
        return this.alert.asObservable();
    }

    clear() {
        this.alert.next();
    }
}
