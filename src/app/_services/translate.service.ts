import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { Lang } from '../_models/lang';
import { AppConfig } from '../app.config';

@Injectable()
export class TranslateService {
    public lang: Lang;
    private translations = {};

    constructor(
        private http: Http,
        private config: AppConfig) {
            this.lang = {active: 'en', alter: 'cz'};
            this.http.get(config.translationsPath).subscribe(res => this.translations = res.json());
    }

    public changeLang() {
        const new_lang_active = this.lang.alter;
        const new_lang_alter = this.lang.active;
        this.lang = { active: new_lang_active, alter: new_lang_alter };
    }

    public getLang(): Lang {
        return this.lang;
    }

    public translate(str: string): string {
        if (this.lang.active === 'cz') {
            if (this.translations[str]) {
                return this.translations[str];
            } else {
                return str;
            }
        } else {
            return str;
        }
    }
}
