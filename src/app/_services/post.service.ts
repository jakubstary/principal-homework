import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { AppConfig } from '../app.config';
import { Post } from '../_models/index';
import 'rxjs/Rx';

@Injectable()
export class PostService {
    constructor(private http: Http, private config: AppConfig) { }

    getAllPosts() {
        return this.http.get(this.config.apiUrl + 'posts').map((response: Response) => response.json());
    }

    getPostById(id: number) {
        return this.http.get(this.config.apiUrl + 'posts/' +  id).map((response: Response) => response.json());
    }

    getCommentsByPostId(id: number) {
        return this.http.get(this.config.apiUrl + 'posts/' +  id + '/comments').map((response: Response) => response.json());
    }

    newPost(post: Post) {
        return this.http.post(this.config.apiUrl + 'posts', post).map((response: Response) => response.json());
    }

    editPost(post: Post) {
        return this.http.put(this.config.apiUrl + 'posts/' + post.id, post).map((response: Response) => response.json());
    }

    deletePost(id: number) {
        return this.http.delete(this.config.apiUrl + 'posts/' + id).map((response: Response) => response.json());
    }
}
