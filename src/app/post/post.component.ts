import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { Post, Comment } from '../_models/index';
import { PostService, AlertService, TranslateService } from '../_services/index';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {
    post: Post = new Post();
    comments: Comment[] = [];

    constructor(private postService: PostService,
        private alertService: AlertService,
        private translateService: TranslateService,
        private route: ActivatedRoute,
        public dialog: MdDialog) {}

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            this.postService.getPostById(params['id']).toPromise().then(post => {
                this.post = post;
                this.postService.getCommentsByPostId(this.post.id).toPromise().then(comments => {
                    this.comments = comments;
                }).catch(exception => {
                    this.alertService.error(this.translateService.translate('Post does not exist or something went wrong!'));
                });
            }).catch(exception => {
                this.post.title = 'Error';
                this.alertService.error(this.translateService.translate('Post does not exist or something went wrong!'));
            });
        }, error => console.log('Error: ', error));
    }

    public openDeleteDialog(post_id: number) {
        const dialogRef = this.dialog.open(DeleteDialog);
        dialogRef.componentInstance.post_id = post_id;
    }

    public openEditDialog(post_id: number) {
        const dialogRef = this.dialog.open(EditDialog);
        dialogRef.componentInstance.post_id = post_id;
    }
}

// Delete dialog
@Component({
  templateUrl: 'delete.dialog.component.html',
})
export class DeleteDialog {
    post_id: number;

    constructor(
        public dialogRef: MdDialogRef<DeleteDialog>,
        private postService: PostService,
        private alertService: AlertService,
        private translateService: TranslateService,
        private router: Router) {}

    onSubmit() {
        this.postService.deletePost(this.post_id).subscribe(data => {
            this.alertService.success(this.translateService.translate('Post deleted successfully'));
            this.dialogRef.close(true);
            window.setTimeout(() => { this.router.navigate(['/']); }, 2000);
        }, error => {
            this.alertService.error(error._body);
        });
    }
}


// Edit Dialog
@Component({
  templateUrl: 'edit.dialog.component.html',
})
export class EditDialog implements OnInit {
    post_id: number;
    newPost: Post = new Post();

    constructor(
        public dialogRef: MdDialogRef<EditDialog>,
        private postService: PostService,
        private alertService: AlertService,
        private translateService: TranslateService) {}

    ngOnInit() {
        this.postService.getPostById(this.post_id).toPromise().then(post => {
            this.newPost = post;
        }).catch(exception => {
            this.alertService.error(this.translateService.translate('Post does not exist or something went wrong!'));
            this.dialogRef.close(true);
        });
    }

    onSubmit() {
        this.postService.editPost(this.newPost).subscribe(data => {
            this.alertService.success(this.translateService.translate('Post updated successfully'));
            this.dialogRef.close(true);
        }, error => {
            this.alertService.error(error._body);
        });
    }
}
