import { PostsAppPage } from './app.po';

describe('posts-app App', () => {
  let page: PostsAppPage;

  beforeEach(() => {
    page = new PostsAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
